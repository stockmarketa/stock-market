<?php

class Controllerauth extends Controller_Template {

    public $template = 'v_auth_base';

    public function before() {
        parent::before();

        $auth = Auth::instance();

        
        $main_menu = new Model_Menumain;
        $main_menu = $main_menu->findall();
        $menu_content = View::factory('v_menu_bg')
                ->bind('main_menu', $main_menu);
        $this->template->block_menu = $menu_content;

        $block_text_intro = View::factory('v_auth_text_intro');
        $this->template->block_text_intro = $block_text_intro;
        
        
        $container = View::factory('v_auth');
        $this->template->block_container = array($container);
        
        $footer_content = View::factory('v_footer');
        $this->template->block_footer = $footer_content;
    }

}
