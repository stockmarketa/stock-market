<?php

class Mycontroller extends Controller_Template {
      public $template = 'v_index'; 	
    public function before() { 
        parent::before(); 
$auth = Auth::instance();
function rus_date() {
// Перевод
 $translate = array(
 "am" => "дп",
 "pm" => "пп",
 "AM" => "ДП",
 "PM" => "ПП",
 "Monday" => "Понедельник",
 "Mon" => "Пн",
 "Tuesday" => "Вторник",
 "Tue" => "Вт",
 "Wednesday" => "Среда",
 "Wed" => "Ср",
 "Thursday" => "Четверг",
 "Thu" => "Чт",
 "Friday" => "Пятница",
 "Fri" => "Пт",
 "Saturday" => "Суббота",
 "Sat" => "Сб",
 "Sunday" => "Воскресенье",
 "Sun" => "Вс",
 "January" => "Января",
 "Jan" => "Янв",
 "February" => "Февраля",
 "Feb" => "Фев",
 "March" => "Марта",
 "Mar" => "Мар",
 "April" => "Апреля",
 "Apr" => "Апр",
 "May" => "Мая",
 "May" => "Мая",
 "June" => "Июня",
 "Jun" => "Июн",
 "July" => "Июля",
 "Jul" => "Июл",
 "August" => "Августа",
 "Aug" => "Авг",
 "September" => "Сентября",
 "Sep" => "Сен",
 "October" => "Октября",
 "Oct" => "Окт",
 "November" => "Ноября",
 "Nov" => "Ноя",
 "December" => "Декабря",
 "Dec" => "Дек",
 "st" => "ое",
 "nd" => "ое",
 "rd" => "е",
 "th" => "ое"
 );
 // если передали дату, то переводим ее
 if (func_num_args() > 1) {
 $timestamp = func_get_arg(1);
 return strtr(date(func_get_arg(0), $timestamp), $translate);
 } else {
// иначе текущую дату
 return strtr(date(func_get_arg(0)), $translate);
 }
 }
;
if($auth->logged_in()==0) $this->redirect('auth');
$userlog = Auth::instance()->get_user();
$userModel = new Model_Myuser;
$count_of_cart = $userModel->count_of_cart();
$get_balance = $userModel->get_balance();
$username = $auth->get_user()->username;
//        $this->_auth = Auth::instance();
$footer_content = View::factory('v_footer');
$user_bg = View::factory('v_user_bg')
        ->bind('username', $username)
        ->bind('count_of_cart', $count_of_cart)
        ->bind('get_balance', $get_balance)
        ;
$main_menu_model = new Model_Menumain;
$main_menu = $main_menu_model->findall();
      $menu_content = View::factory('v_menu_bg')
          ->bind('main_menu', $main_menu);
$this->template->userlog = $userlog;
$this->template->block_footer = $footer_content;
$this->template->block_menu = $menu_content;
$this->template->user_bg = $user_bg;

//if ($auth->logged_in('user')!=1)$this->redirect('netprav');
}
}          