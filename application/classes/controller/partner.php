<?php

class Controller_Partner extends Mycontroller {

    public function action_index() {

        $domain = URL::base('http', TRUE);
        $urlreg = $domain . 'auth/registration';
        $regcode = new Model_Regcode;
        $allcode = $regcode->list_code();

        //echo 'kk';
        $partner = View::factory('v_partner_container')
                ->bind('allcode', $allcode)
                ->bind('urlreg', $urlreg)
        ;
        $this->template->block_content = array($partner);
    }

    public function action_deletecode() {
        $error = true;
        $modelRegcodes = new Model_Regcode;
        $id = (int) $this->request->param('id');

        if ($id) {
            $r = $modelRegcodes->delete_code($id);
            $error = FALSE;
            if ($r > 0)
                $error = TRUE;
        }
        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array('error' => true, 'message' => 'Ошибка при удалении'); // по умолчанию возвращаем код с ошибкой
            } else {


                // если валидация прошла успешно и запись в БД новой статьи прошла успешно
                $result['error'] = false; // возвращаем код успеха!

                $data = $modelRegcodes->list_code();  // выбираем список статей из БД и формируем масив
                if ($data) {
                    foreach ($data as $v) {
                        $result['content'][] = array(
                            'id' => $v['id'],
                            'code' => $v['code'],
                            'status' => $v['status']
                        );
                    }
                }
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            //$this->redirect('/articles'); // если запрос был не Аяксом, то редиректим на страницу списка статей
            $this->redirect('/partner');
        }
    }

    public function action_add() {
        $auth = Auth::instance();
        $userid = Auth::instance()->get_user();
        $error = true;
        $id = null;
        $modelRegcodes = new Model_Regcode;
        $code = $modelRegcodes->generateCode(40);
        $data = array(
            'code' => $code,
            'add_user_id' => $userid,
        );
        $id = $modelRegcodes->savecode($data); //добавляем в БД запись
        if ($id) {
            $error = false;
        }


        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array('error' => true, 'message' => 'Ошибка валидации формы!'); // по умолчанию возвращаем код с ошибкой
            } else {
                // если валидация прошла успешно и запись в БД новой статьи прошла успешно
                $result['error'] = false; // возвращаем код успеха!

                $data = $modelRegcodes->list_code($id);  // выбираем список статей из БД и формируем масив
                if ($data) {
                    foreach ($data as $v) {
                        $result['content'][] = array(
                            'id' => $v['id'],
                            'code' => $v['code'],
                            'status' => $v['status']
                        );
                    }
                }
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            //	$this->redirect('/articles'); // если запрос был не Аяксом, то редиректим на страницу списка статей
            $this->redirect('/partner');
        }
    }

}
