<?php

class Controller_Cart extends Mycontroller {

    public function action_index() {
        $cartModel = new Model_Cart();
        $all_id_in_cart = array();
        $all_id_in_cart = $cartModel->getAllInCart();
        $cart = View::factory('v_cart')
                ->bind('result', $all_id_in_cart)
        ;
        $this->template->block_content = array($cart);
    }

    public function action_delete() {
        $error = true;
        $cartModel = new Model_Cart();
        $userModel = new Model_Myuser();
        $id_img = (int) $this->request->param('id');

        if ($id_img) {

            $d = $cartModel->delete_img_cart($id_img);
            $result['count_of_cart'] = $userModel->count_of_cart();
            $result['error'] = false;
            $error = FALSE;
            if (empty($d))
                $error = TRUE;
        }
        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array('error' => true, 'message' => 'Ошибка при удалении'); // по умолчанию возвращаем код с ошибкой
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            $this->redirect('/cart');
        }
    }

    public function action_add() {
        
        $error = TRUE;
        $userModel = new Model_Myuser;
        $id = (int) $this->request->param('id');
        $addtocart = $userModel->add_to_cart($id);
        if ($addtocart) {

            $count_of_cart = $userModel->count_of_cart();
            $error = FALSE;
        } else {
            
        }

        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array(
                    'error' => true,
                    'message' => 'Ошибка при добавлении, вожможно изображение уже в корзине'
                ); // по умолчанию возвращаем код с ошибкой
            } else {
                $result['error'] = false;
                $result['message'] = 'В корзине';
                $result['count_of_cart'] = $count_of_cart;
                $result['content'][] = array(
                    'content' => 'content',
                );
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            //$this->redirect('/articles'); // если запрос был не Аяксом, то редиректим на страницу списка статей
            $this->redirect('/');
        }
    }
    public function action_pay() {
        $result = array();
        $error = true;
        $settings = New Model_Settings;
        $orders = New Model_Orders;
        $cartModel = new Model_Cart();
        $userModel = new Model_Myuser();
        $shopdiz = new Model_Shopdiz();
        $image = new Model_Images();

        if ($post = $this->request->post()) {
            $ids = $post['id_image'];
            $user_balance = $userModel->get_balance();
            $count_pay_photo = count($ids);
            $price_img = $settings->where('id', '=', 1)->find()->value;
            $sum_order = $count_pay_photo * $price_img;
            if ($user_balance < $sum_order) {
                $message = 'Недостаточно средств';
                $error = TRUE;
            } else {
                $balanse_in_shopdiz = $shopdiz->get_balance();



                // print_r($ids);


                foreach ($ids as $id) {// по всем нашим ид с корзины
                    $in_base = $image->checkImageInBase($id);
                    if (!$in_base) {//нету в базе
                        $url_download = $shopdiz->check_in_history($id);
                        if ($url_download) {//есть в истории заказов шопдиза
                            $folder = $image->createFolder($id);
                            $shopdiz->downloadFromHistory($url_download, $id, $folder);
                        } else {//нету в базе и нету в купленых

                            //$shopdiz->
                            //проверяем есть ли в корзине
                            //добавляем в корзину если нету
                            //покупаем картинку
                        
                            $new_image_id[] = $id;
                        }
                    } else {//есть в базе путь 
                         
                    }
                }
                $data_post_array = $shopdiz->get_format_info_arr($new_image_id);
                $shopdiz->add_to_cart($data_post_array); 
                 $shopdiz->get_id_shopdiz_in_cart($new_image_id); 

                 $cartModel->delete_img_cart_all();
                print_r($new_image_id);
                $result['count_of_cart'] = $userModel->count_of_cart();
                $result['error'] = FALSE;
                $error = FALSE;
            }
        }

        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array('error' => true, 'message' => $message); // по умолчанию возвращаем код с ошибкой
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {

            $this->redirect('/cart');
        }
    }

}
