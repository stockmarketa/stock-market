<?php

class Controller_Account extends Mycontroller {

    public function action_index() {
         $data = array();
        $myuser = new Model_Myuser();
        $data['user']['username'] = $myuser->displayusername();
        $data['user']['email'] = $myuser->getUserEmail();
        if (isset($_POST['btnsubmit'])) {
            $oldpass = Arr::get($_POST, 'oldpass', '');
            $newpass1 = Arr::get($_POST, 'newpass1', '');
            $newpass2 = Arr::get($_POST, 'newpass2', '');

            if ($myuser->saveNewPass($oldpass, $newpass1, $newpass2)) {
                $data['newpassok'] = '';
            } else {
                $data['errors'] = $myuser->getErrors();
                $myuser->getErrors();
            }
        }
        $user_container = View::factory('accountview')
                ->bind('data', $data['errors'])
                ->bind('info', $data['user'])
                ->bind('newpassok', $data['newpassok'])
                
        ;
        $this->template->block_content = array($user_container);
    }

    public function action_orders() {
             $orders_container = View::factory('v_orders')
        ;
        $this->template->block_content = array($orders_container);         
    }

    public function action_payments() {
        $pay_methods=ORM::factory('paymentmethod')
                ->where('status', '=', 1)
                ->find_all();
             $orders_container = View::factory('v_payments')
                     ->bind('pay_methods', $pay_methods)
        ;
        $this->template->block_content = array($orders_container); 
    }


    

    public function action_logout() {
        Auth::instance()->logout();
        $this->redirect('/');
    }

}
