<?php

class Controller_Main extends Mycontroller {

    public function action_index() {
        $id_img = '';
        $news = New Model_News();
        $allNews = $news->order_by('date','ASC')->find_all();
        
        $search_line = View::factory('v_search_line')
                ->bind('id_img', $id_img);
        $this->template->block_content = array(
            View::factory('v_banner')
                ->bind('id_img', $id_img)
                ->bind('search_line', $search_line),
            View::factory('v_news_container')
                ->bind('allNews', $allNews));
    }

    public function action_addtocart() {
        $error = TRUE;
        $userModel = new Model_Myuser;
        $id = (int) $this->request->param('id');
        $addtocart = $userModel->add_to_cart($id);
        if ($addtocart) {

            $count_of_cart = $userModel->count_of_cart();
            $error = FALSE;
        } else {
            
        }

        if (Request::initial()->is_ajax()) { // выполняем только если запрос был через Ajax
            if ($error) {
                $result = array(
                    'error' => true,
                    'message' => 'Ошибка при добавлении, вожможно изображение уже в корзине'
                ); // по умолчанию возвращаем код с ошибкой
            } else {
                $result['error'] = false;
                $result['message'] = 'В корзине';
                $result['count_of_cart'] = $count_of_cart;
                $result['content'][] = array(
                    'content' => 'content',
                );
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            //$this->redirect('/articles'); // если запрос был не Аяксом, то редиректим на страницу списка статей
            $this->redirect('/');
        }
    }

}
