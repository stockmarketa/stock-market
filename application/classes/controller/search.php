<?php

class Controller_Search extends Mycontroller {

    public function action_index() {
        $result = array();
        $error = TRUE;
        $shopdiz = new Model_Shopdiz();
        $model_img = new Model_Images();
        $usermodel = new Model_Myuser();
        if ($post = $this->request->post()) {
            if ($post['stock_id'] == 1) {
                $stock_id = $post['stock_id'];
                $url = $model_img->getUrlShutter($post['search_image']);

                if (!$url) {
                    $message = 'Плохой ИД или URL';
                } else {
                    $arrNameUrlPre = $model_img->getUrlImageNameImageShutterPreview($url);
                    // print_r($arrNameUrlPre);
                    //echo $url;
                    //$name = $model_img->grab_name_image_shuter($url);
                    //$format_id = $post['format_id'];

                    $id_img = $model_img->getId($stock_id, $arrNameUrlPre['name']);

                    if ($id_img) {


                        $PhotoInCart = $usermodel->PhotoInCart($id_img); //проверяем есть ли в корзине по id полученному из getId
                        if (!$PhotoInCart) {// если  нету в корзине и есть в базе (сток ид и имя рисунка) 
                            $error = FALSE;
                        } else {
                            $message = 'Это изображение уже в корзине';
                        }
                    } else {//если такого изображения нет в таблице
                        if ($arrNameUrlPre) {//если имя спарсилось из шатерстока
                            $formats = $shopdiz->radio_hidden_load(array($arrNameUrlPre['name']));
                            $id_img = $model_img->addToImageFromShuter($arrNameUrlPre['name'], $stock_id, $arrNameUrlPre['urlimage'], $formats); // 
                        }
                        if ($id_img) {
                            //$urlImagePre =  $Model_Image->getUrlImageShutterPreview($urlimage, $all_path);
                            $error = FALSE;
                            $message = '';
                        } else {
                            $message = 'Ошибка';
                        }
                    }
                    if (!$error) {
                        $result = $model_img->getImageInfo($id_img);
                    } else {
                        $error = TRUE;
                        $result = $message;
                    }
                }
            } else {
                return false;
            }
        }


        if (Request::initial()->is_ajax()) {
            if ($error) {
                $result = array('error' => true, 'message' => $message); // по умолчанию возвращаем код с ошибкой
            }
            header('Content-Type: text/json; charset=utf-8');  // Устанавоиваем правильный заголовок
            echo json_encode($result);  // на выходе отдаем код в формате JSON
            exit;
        } else {
            //	$this->redirect('/articles'); // если запрос был не Аяксом, то редиректим на страницу списка статей
            //$this->redirect('/');
            echo 'NO AJAX';
        }



        if (isset($_POST['simple_html'])) {
            $url = 'http://www.shutterstock.com/pic-' . $_POST['id'];

            $html = file_get_html($url);
            $ret = $html->find('a[class=preview-modal-trigger]');
            if (count($ret)) {
                foreach ($ret as $value) {
                    $urlimage = 'http:' . $value->href;
                }
            }
            $html->clear();
            unset($html);
            $path_parts = pathinfo($urlimage);
            $name = $path_parts['basename'];
            $dir = 'photos/';
            $all_path = $dir . $name;
            $model_img->grab_image($urlimage, $all_path);
            $src = '<img src="' . $all_path . '"/>';
        }
    }

}
