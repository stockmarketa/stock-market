<?php

class Controller_News extends Mycontroller{
 	 
    public function action_index() {
        $news = New Model_News();
        $allNews = $news->order_by('date','ASC')->find_all();
        
          
        $this->template->block_content = array(View::factory('v_news_container')->bind('allNews', $allNews));

    }
}