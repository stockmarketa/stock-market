<?php

class Controller_Ajax extends Controller {

    public function action_emailunique() {
        $email = Arr::get($_POST, 'email', '');

        $myuser = new Model_Myuser();
        $res = $myuser->username_unique($email);

        echo json_encode(array('result' => $res));
    }

    public function action_checkOldPass() {
        $oldpass = Arr::get($_POST, 'oldpass', '');

        $myuser = new Model_Myuser();
        $res = $myuser->checkOldPass($oldpass);

        echo json_encode(array('result' => $res));
    }
    public function action_countcartproduct() {
$userModel = new Model_Myuser;
$count_of_cart = $userModel->count_of_cart();
        echo json_encode(array('result' => $count_of_cart));
    }
    public function action_getbalance() {
$userModel = new Model_Myuser;
$get_balance = $userModel->get_balance();
        echo json_encode(array('result' => $get_balance));
    }


}
