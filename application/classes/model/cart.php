<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Cart {

    public function delete_img_cart_all() {
        $userId = Auth::instance()->get_user();
        $cart = ORM::factory('myuser', $userId)->cart;
        if (count($cart) > 0) {
            $cart_n = ORM::factory('myuser', $userId);
            $cart_n->cart = null;
            $cart_n->save();
             return TRUE;
        }
        return array();
    }
    public function delete_img_cart($img_id) {
   $userId = Auth::instance()->get_user();
   
   $cart = ORM::factory('myuser', $userId)->cart;
        if (count($cart) > 0) {
            $id_str='';
            $idsphoto = explode(',', $cart);
            array_pop($idsphoto);
             foreach ($idsphoto as $value) {
                 if($value==$img_id) continue;
                $id_str .= $value.',';
            }
             $cart_n = ORM::factory('myuser', $userId);
             $cart_n->cart = $id_str;
             $cart_n->save();
            return TRUE;
        }
        return array();
       

    }


    public function getAllInCart() {
        
        $userId = Auth::instance()->get_user();
        $cart = ORM::factory('myuser', $userId)->cart;
        if ($cart == '')            return array();
        if (count($cart) > 0) {
            $idsphoto = explode(',', $cart);
            array_pop($idsphoto);
             foreach ($idsphoto as $value) {
                $img = ORM::factory('Images', $value);
                
                $format_info = json_decode($img->format_info);
                $result[] = array(
                    'id' => $img->id,
                    'name' => $img->name,
                    'preview_rout' => $img->preview_rout,
                    'format_info' => $format_info,
                    'size' => $format_info->size,
                );
            }
            return $result;
        }
        return array();

        //return ORM::factory('myuser', $userId)->cart;
    }

}
