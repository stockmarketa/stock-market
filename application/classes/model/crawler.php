<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Crawler {

    private static $loginURL = 'https://shopdiz.biz/user/login';
    private static $username = 'egoruz';
    private static $password = 'ERER663752ERER';
    private static $headers = array(
        'Host: shopdiz.biz',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Connection: Keep-Alive'
    );
    private static $userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0';
    private static $cookieFile = null;
    private $CSRFToken = null;
    private $curl = null;

    public function __construct() {
        self::$cookieFile = $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt';

        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, self::$headers);
        curl_setopt($this->curl, CURLOPT_HEADER, true);
        curl_setopt($this->curl, CURLOPT_USERAGENT, self::$userAgent);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, self::$cookieFile);
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, self::$cookieFile);
    }

    public function getCSRFToken() {
        
        if ($this->CSRFToken !== null) {
            return ;
        }

        curl_setopt($this->curl, CURLOPT_URL, self::$loginURL);
        curl_setopt($this->curl, CURLOPT_POST, false);

        $data = curl_exec($this->curl);
        
        preg_match('/name="_csrf" value="(.*)"/', $data, $matches);

        if (isset($matches[1]) && empty($matches[1]) === false) {
           
            return    $this->CSRFToken = $matches[1];

        } else {
            
            return 'Unable to extract CSRF Token';
//            throw new Exception('Unable to extract CSRF Token');
        }
    }

}
