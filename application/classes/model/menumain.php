<?php defined('SYSPATH') or die('No direct script access.');

class Model_Menumain extends ORM
{
	protected $_table_name = 'menumain';
        public function findall() { 
            $auth = Auth::instance();
             $is_login = $auth->logged_in();
             $menu = ORM::factory($this->_table_name)
                     ->order_by('sort');
             if ($is_login == 1){
                 $menu = $menu ->where('login', '=', 1)->find_all();
             }
 else {
     $menu = $menu ->where('no_login', '=', 1)->find_all();
 }
        //return parent::find_all();
            return $menu;
        }
}