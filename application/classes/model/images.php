<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Images extends ORM {

    protected $_table_name = 'images';
    protected $errors = array();
    protected $_primary_key = 'id';
  protected $_belongs_to = array(
      'stock'    => array(
               'model'       => 'stocks',
               'foreign_key' => 'stock_id',
          
            )
    );

    function checkImageInBase($id) {
        $rout = ORM::factory($this->_table_name, $id)
                ->rout;
        if ($rout != null) {
            return TRUE;
        }
        return FALSE;
    }

    function createFolder($id_img) {
        
        $number = 10;
        $id_stock = ORM::factory($this->_table_name,$id_img)->stock_id;
        $stock_name = ORM::factory('stocks', $id_stock)->name;
        
        //$number - кол-во символов в коде
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0');

        // Генерируем код
        $code = "";
        for ($i = 0; $i < $number; $i++) {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $code .= $arr[$index];
        }

       

        if (!is_dir('photos'))
            mkdir("photos", 0700);
        if (!is_dir('photos/' . $stock_name)) {
            mkdir('photos/' . $stock_name, 0700);
            mkdir('photos/' . $stock_name . '/image', 0700);
            mkdir('photos/' . $stock_name . '/preview', 0700);
        }
        if (!is_dir('photos/' . $stock_name.'/image')) {
             mkdir('photos/' . $stock_name . '/image', 0700);
        }
        if (!is_dir('photos/' . $stock_name.'/preview')) {
             mkdir('photos/' . $stock_name . '/preview', 0700);
        }
        $dirs = scandir('photos/' . $stock_name . '/image');
        foreach ($dirs as $value) {
            if ($value == '.' || $value == '..')
                continue;
            $id_img_folder = explode('_', $value);
            if (isset($id_img_folder[1]) && $id_img_folder[1] != '' && $id_img == $id_img_folder[1]) {
                return 'photos/' . $stock_name . '/image/'.$value.'/';
            }
        }

        if (!is_dir('photos/' . $stock_name . '/image/')) {
            
        }
$folder_name = 'photos/'.$stock_name.'/image/'.$code . '_' . $id_img.'/';
mkdir ($folder_name);
        return $folder_name;
    }

    function checkURLExists($url = "") {
        if (empty($url)) return FALSE;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != 200) return false;
        return true;

    }

//    function get_http_response_code($url) {
//
//        if (!empty($url_p ['host']) and checkdnsrr($url_p ['host'])) {
//            return FALSE;
//        } else {
//            $headers = @get_headers($url);
//            if (substr($headers[0], 9, 3) == 200) {
//
//                return TRUE;
//            } else {
//                return FALSE;
//            }
//        }
//    }

    public function getUrlImageNameImageShutterPreview($url) {

        if (empty($url))
            return FALSE;
            include_once 'simple_html_dom.php';
            $html = file_get_html($url);
           // echo $html;
        $u = $html->find('div[class=js-preview-crop] a',0);
        $n = $html->find('a[class=a js_lightbox_add]',0);
//            $n = $html->find('button[class=add-to-lightbox-trigger]');
//            $u = $html->find('img[itemprop=image]');
            //$urlimage = $html->find('img[itemprop=image]');

            if (count($n) && count($u)) {
                $name = $n->attr['data-lightboximageid'];
                $urlimage = $u->attr['href'];
//                $name = $n[1]->attr['data-image-id'];
//                $urlimage = 'http:' . $n[0]->src;
                //$urlimage = 'http:' . $ret[0]->src;
                return (array('name' => $name, 'urlimage' => $urlimage));
            } else {
                return FALSE;
            }
            $html->clear();
            unset($html);
            // file_get_contents('http://somenotrealurl.com/notrealpage');

    }
    public function getUrlImageNameImageShutterPreviewkk($url) {

        if (empty($url))
            return FALSE;
            include_once 'simple_html_dom.php';
            $html = file_get_html($url);
           // echo $html;
        $n = $html->find('div[class=js-preview-crop] a',0);
        $u = $html->find('a[class=a js_lightbox_add]',0);
//            $n = $html->find('button[class=add-to-lightbox-trigger]');
//            $u = $html->find('img[itemprop=image]');
            //$urlimage = $html->find('img[itemprop=image]');
            //if (count($n)) {

                $name = $n->attr['href'];
                $urlimage = $u->attr['data-lightboximageid'];
                //$urlimage = 'http:' . $ret[0]->src;
                echo $name.'---'.$urlimage;
return;
                return (array(
                    'name' => $name,
                    'urlimage' => $urlimage
                ));
//            } else {
//                return FALSE;
//            }
            $html->clear();
            unset($html);
            // file_get_contents('http://somenotrealurl.com/notrealpage');

    }

//    public function getUrlImageShutterPreview($url) {
//        if (empty($url))
//            return FALSE;
//        if (!$this->checkURLExists($url)) {
//            return FALSE;
//        } else {
//            include_once 'simple_html_dom.php';
//            $html = file_get_html($url);
//            //$ret = $html->find('a[class=preview-modal-trigger]');
//            $ret = $html->find('img[itemprop=image]');
//            if (count($ret)) {
//                //$urlimage = 'http:' . $ret[0]->href;
//                $urlimage = 'http:' . $ret[0]->src;
//                return $urlimage;
//            } else {
//                return FALSE;
//            }
//            $html->clear();
//            unset($html);
//            // file_get_contents('http://somenotrealurl.com/notrealpage');
//        }
//    }
    public function getUrlShutter($string) {//http://www.shutterstock.com/cat.mhtml?autocomplete_id=&language=ru&lang=ru&search_source=&safesearch=1&version=llv1&searchterm=jj&media_type=images&media_type2=images&searchtermx=&photographer_name=&people_gender=&people_age=&people_ethnicity=&people_number=&color=&page=1&inline=351569195
        if (is_numeric($string)) {
            $url = 'http://www.shutterstock.com/pic-' . $string;
            if ($this->checkURLExists($url)) {
                return $url;
            } else {
                $url = FALSE;
            }
        } elseif (filter_var($string, FILTER_VALIDATE_URL)) {//если url
            if ($this->checkURLExists($string)) {//если 200
                $url = $string;
                preg_match('/pic-([^\/]*)/', $url, $matches);
                if (isset($matches[0])) {
                    $url = 'http://www.shutterstock.com/' . $matches[0];
                } else {
                    $arr = explode('=', $string);
                    $id = end($arr);
                    $url = 'http://www.shutterstock.com/pic-' . $id;
                }
            } else {
                $url = FALSE;
            }
        } else {
            $url = FALSE;
        }

        return $url;
    }

//    public function grab_name_image_shuter($url) {
//        if (empty($url))
//            return FALSE;
//        if (!$this->checkURLExists($url)) {
//            return FALSE;
//        } else {
//            include_once 'simple_html_dom.php';
//            $html = file_get_html($url);
//            $ret = $html->find('button[class=add-to-lightbox-trigger]');
//            if (count($ret)) {
//                $name = $ret[0]->attr['data-image-id'];
//                return $name;
//            } else {
//                return FALSE;
//            }
//            $html->clear();
//            unset($html);
//            // file_get_contents('http://somenotrealurl.com/notrealpage');
//        }
//    }

    public function grab_image($url, $saveto) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }

    public function getImageInfo($id) {
        if ($id >= 1) {

            $img = ORM::factory('images', $id);

            $format_info = json_decode($img->format_info);
            $result = array(
                'id' => $img->id,
                'name' => $img->name,
                'preview_rout' => $img->preview_rout,
                'format_info' => $format_info,
                'size' => $format_info->size,
            );
            return $result;
        } else {
            return FALSE;
        }
    }

    public function addToImageFromShuter($name, $stock_id, $urlpreimg, $format_info) {

        $preview_rout_local = 'photos/shutterstock/preview/preview-' . $name . '.jpg';
        //$preview_rout_shuter = $this->getUrlImageShutterPreview($url);
        $this->grab_image($urlpreimg, $preview_rout_local);



        $add = ORM::factory($this->_table_name);
        $add->name = $name;
        $add->stock_id = $stock_id;
        $add->preview_rout = $preview_rout_local;
        $add->format_info = $format_info;

        try {
            $add->save();
            return $add->id;
        } catch (Exception $ex) {

            return $this->errors;
        }
    }

    public function getURL($stock, $search_image) {
        if ($stock == 1) {
            return 1;
        } elseif ($stock == 2) {
            return 2;
        } else {
            return FALSE;
        }
    }

    public function getName($stock, $name) {
        return $name;
    }

    public function getId($stock, $name) {
        $image = ORM::factory($this->_table_name, array('stock_id' => $stock, 'name' => $name));
        if ($image->loaded()) {
            return $image->id;
        } else {
            return FALSE;
        }
    }

    public function getErrors() {
        return $this->errors;
    }

}
