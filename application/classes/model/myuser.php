<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Myuser extends ORM {

    protected $_table_name = 'users';
    protected $errors = array();
 
 protected $_has_many = array(
      'ordersss'    => array(
               'model'       => 'orders',
               'foreign_key' => 'user_id',
          
            )
    );
    
    
    public function rules() {
        return array(
            'username' => array(
                array('not_empty'),
                array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
                //array('email'),
                array('min_length', array(':value', 4)),
                array(array($this, 'username_unique')),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array($this, 'email_unique')),
            ),
            'password' => array(
                array('not_empty'),
                array('min_length', array(':value', 5)),
            ),
        );
    }

    public function username_unique($username) {
        $db = Database::instance();

        if ($this->id) {
            $query = 'SELECT id
                    FROM users
                    WHERE id != ' . $this->id . ' AND username = ' . $db->escape($username);
        } else {
            $query = 'SELECT id
                    FROM users
                    WHERE username = ' . $db->escape($username);
        }

        $result = $db->query(Database::SELECT, $query, FALSE)->as_array();
        if (count($result) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function email_unique($email) {
        $db = Database::instance();

        if ($this->id) {
            $query = 'SELECT id
                    FROM users
                    WHERE id != ' . $this->id . ' AND email = ' . $db->escape($email);
        } else {
            $query = 'SELECT id
                    FROM users
                    WHERE username = ' . $db->escape($email);
        }

        $result = $db->query(Database::SELECT, $query, FALSE)->as_array();
        if (count($result) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function PhotoInCart($id) {
        $userId = Auth::instance()->get_user();
        $all_id_photo_in_cart = ORM::factory('myuser', $userId)->cart;
        $all_arr = explode(',', $all_id_photo_in_cart);
//        print_r($all_arr);
//        echo $id;
        if ($id >= 1 && in_array($id, $all_arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function add_to_cart($img_id) {
        //$model_img = new Model_Image();

        if (!$this->PhotoInCart($img_id) && $img_id >= 1) {
            $userId = Auth::instance()->get_user();
            $user = ORM::factory('myuser', array('id' => $userId));
            $user->cart = $user->cart . $img_id . ',';
            $user->save();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_balance() {
        $auth = Auth::instance();
        $userId = $auth->get_user();
        $balance = ORM::factory('myuser', $userId)->balance;
        return $balance;
    }
    public function count_of_cart() {
        $auth = Auth::instance();
        $userId = $auth->get_user();
        $cart = ORM::factory('myuser', $userId)->cart;
        $cart = explode(',', $cart);
        $count = count($cart) - 1;
        return $count;
    }

    public function getusername($userId) {
        return ORM::factory('myuser', $userId)->username;
        ;
    }

    public function getUserEmail() {
        $auth = Auth::instance();
        $userId = $auth->get_user();
        return ORM::factory('myuser', $userId)->email;
        ;
    }

    public function displayusername() {
        $auth = Auth::instance();
        $userId = $auth->get_user();

        $usertemp = ORM::factory('myuser', array('id' => $userId));

        return $usertemp->username;
    }

    public function checkOldPass($oldpass) {
        $auth = Auth::instance();
        return $auth->check_password($oldpass);
    }

    public function saveNewPass($oldpass, $newpass1, $newpass2) {
        $vData = array("oldpass" => $oldpass, "newpass1" => $newpass1, "newpass2" => $newpass2,);

        $validation = Validation::factory($vData);
        $validation->rule('oldpass', 'not_empty');
        $validation->rule('oldpass', 'alpha_numeric');
        $validation->rule('oldpass', array($this, 'checkOldPass'));
        $validation->rule('newpass1', 'not_empty');
        $validation->rule('newpass1', 'alpha_numeric');
        $validation->rule('newpass1', 'matches', array(':validation', 'newpass1', 'newpass2'));

        if (!$validation->check()) {
            $this->errors = $validation->errors('catErrors');
            return FALSE;
        }

        $auth = Auth::instance();
        $userId = $auth->get_user();

        $usertemp = ORM::factory('myuser', array('id' => $userId));
        $usertemp->password = $auth->hash_password($newpass1);
        $usertemp->save();

        return TRUE;
    }

    public function getErrors() {
        return $this->errors;
    }

}
