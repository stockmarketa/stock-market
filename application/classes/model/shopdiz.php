<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Shopdiz extends ORM {

    protected $_table_name = 'shopdiz';
private static $headers = array(
        'cache-control:no-store, no-cache, must-revalidate',
        'content-encoding:gzip',
        'content-type:text/html; charset=UTF-8',
    );
    public function auth_check($data) {
        if (preg_match("/logout/i", $data))
            return TRUE;
        return FALSE;
    }

    public function downloadFromHistory($url = null, $id_img, $dir) {//рабочая загрузка 
        if ($url == null)
            return FALSE;
        $arrurl = explode("_", $url);
        $type = end($arrurl);
        $image = ORM::factory('images', $id_img);
        $name = $image->stock->name . '_' . $image->name . '.' . $type;
        $rout = $dir . $name;
        $fp = fopen($rout, 'w');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILE, $fp);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        $r = curl_exec($ch);
        curl_close($ch);
        $image->rout = $rout;
        $image->save();
    }

    public function get_sizes($imgIdShutter = null) {
        $url = 'https://shopdiz.biz/ajax/get-sizes';
        $arrayPost = array(
            'url'=>$imgIdShutter,
            'input_id'=>0,
            'stock_name'=>'shutterstock',
        );
$httpBuildPost = http_build_query($arrayPost);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::$headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'user-agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36');
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'url=647728963&input_id=0&stock_name=shutterstock');
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        $data = curl_exec($ch);
        return $data;
        
    }
    public function get_id_shopdiz_in_cart($id_img = array()) { //возвращает id  шопдиза по нащей id из корзины для покупки
        if ($id_img) {
             include_once 'simple_html_dom.php';

 
           
            $ch = curl_init('https://shopdiz.biz/cart');
            curl_setopt($ch, CURLOPT_USERAGENT, 'IE20');
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, '1');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
            curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
            $str = curl_exec($ch);
            

            
            
            $html_base = new simple_html_dom();
            $html_base->load($str);
            $form = $html_base->find('form[id=cart-form] table tbody tr td input[name=items[]]');
            foreach ($form as $element => $e) {
                $tr = $html_base->find('tr[id=cart-tr-item-' . $e->value . '] td[5]');
                foreach ($tr as $element2 => $i) {
                    $idshuter_parse = str_replace(' ', '', $i->innertext);
                    foreach ($id_img as $value) {
            $image = ORM::factory('images', $value);
            $id_shuter = $image->name;
                    
                    if ($idshuter_parse == $id_shuter) {

                        $image->shopdiz_id = $e->value;
                        $image->save();
                       
                    }
                    }
                   
                }
            }
            return TRUE;
        }
        $html_base->clear();
        unset($html_base);
        return FALSE;
    }

    public function check_in_history($id) {//<a class="normal_link" href="/download/singleFile/hid/534591/url/289903277/stock/shutterstock/type/vector_eps">Скачать</a>
        $shuter_id = ORM::factory('images', $id)
                ->name;
        $find_text = 'shutterstock ' . $shuter_id;
        $ch = curl_init('https://shopdiz.biz/history');
        curl_setopt($ch, CURLOPT_USERAGENT, 'IE20');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, '1');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        $str = curl_exec($ch);
        if (preg_match("/" . $find_text . "/i", $str)) {
            include_once 'simple_html_dom.php';
            $html = new simple_html_dom();
            $html->load($str);
            $arrfind = $html->find('div[class=row-items]');
            foreach ($arrfind as $i => $value) {
                $html1 = new simple_html_dom();
                $html1->load($value);
                $arrfind1 = $html1->find('div div, a');
                foreach ($arrfind1 as $value2) {
                    if (str_replace(' ', '', $value2->innertext) == $shuter_id) {
                        $eee = 1;
                    }
                    if ($value2->href != '' && isset($eee)) {
                        $download_url = 'https://shopdiz.biz' . $value2->href;
                        unset($eee);
                        break;
                    }
                }
            }
            $html->clear();
            unset($html);
            return $download_url;
        }
        return FALSE;
    }

    public function get_balance() { // работает
        include_once 'simple_html_dom.php';
        
        $ch = curl_init('https://shopdiz.biz');
        curl_setopt($ch, CURLOPT_USERAGENT, 'IE20');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, '1');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        $text = curl_exec($ch);
        curl_close($ch);
       // echo $text;
        $html = new simple_html_dom();
        $html->load($text);
        $balance = $html->find('div[id=user-stats] div div a span',0);
        if(isset($balance)&& $balance!=''){
            $balance = preg_replace('/[^0-9.]/', '', $balance->innertext);
            if (is_numeric($balance)) {
            return $balance;}
 else {
    return 'no numeric';
 }
        }
        $html->clear();
        unset($html);
        return 'no extract balance';
    }

    public function get_format_info_arr($ids = array()) {// 
//            'data[0]' => '{"type":"image","url":"http:\/\/www.shutterstock.com\/pic-123.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.∼13.9 MB"}',
//            'data[1]' => '{"type":"image","url":"http:\/\/www.shutterstock.com\/pic-43211302.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 8576x5696 | approx.∼41.6 MB"}'
//        );
        foreach ($ids as $i => $value) {

            $format = ORM::factory('images', $value)->format_info;
            $send['data[' . $i . ']'] = $format;
        }
        return $send;
    }

    public function radio_hidden_load($id_suter_img_arr) {
        return array ('{"type":"image","url":"http:\/\/www.shutterstock.com\/pic-123.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.∼13.9 MB"}');
        include_once 'simple_html_dom.php';
        $str_id = '';
        $count_arr = count($id_suter_img_arr);
        foreach ($id_suter_img_arr as $c => $id) {
            $br = "\r\n";
            if ($c == $count_arr - 1){
            $br = '';}
            $str_id .= $id . $br;
        }
//echo '~'.$str_id.'~';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest"));
//        curl_setopt($ch, CURLOPT_URL, 'https://shopdiz.biz/ajax/multiGetSizes');
        curl_setopt($ch, CURLOPT_URL, 'https://shopdiz.biz/ajax/multi-get-size');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_REFERER, 'https://shopdiz.biz/cp/addlinks/shutterstock');
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'AddMultiForm[photostock]' => 'shutterstock',
            'AddMultiForm[links]' => $str_id, //123\r\n221809534
        ));
        $data = curl_exec($ch);
        //$info = curl_getinfo($ch);
        curl_close($ch);
        //echo $data;
        $html_base = new simple_html_dom();
        $html_base->load($data);
        $form = $html_base->find('input');
        foreach ($form as $element => $i) {
            $formats[$i->name] = $i->value;
        }
        $html_base->clear();
        unset($html_base);

        return $formats;
    }

    public function pay($ids) {
       // items[]=1046493
        //https://shopdiz.biz/cart/partBuy
        foreach ($ids as $i => $value) {
$idShopdiz = ORM::factory('images',$value)->shopdiz_id;
$send['items['.$i.']'] =$idShopdiz;
 }
                $ch = curl_init();
      //  curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest"));
        curl_setopt($ch, CURLOPT_URL, 'https://shopdiz.biz/cart/partBuy');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_REFERER, 'https://shopdiz.biz/cart');
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $send);
        $data = curl_exec($ch);
        //$info = curl_getinfo($ch);
        curl_close($ch);
        return TRUE;
        
    }
    public function add_to_cart($data_post_array) {//$data_post_array = array('data[0]'=>'{"type":"image","url":"http:\/\/www.shutterstock.com\/pic-123.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.∼13.9 MB"}');
        $url = ORM::factory($this->_table_name, 4)->value; //https://shopdiz.biz/cart/add/shutterstock
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); // таймаут4
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // просто отключаем проверку сертификата
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        curl_setopt($ch, CURLOPT_POST, 1); // использовать данные в post
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post_array);
        $data = curl_exec($ch);
        curl_close($ch);
    }

    public function auth_user() {
$CSRFTokenModel = new Model_Crawler();

        $url_login = ORM::factory($this->_table_name, 1)->value;
        $username = ORM::factory($this->_table_name, 2)->value;
        $password = ORM::factory($this->_table_name, 3)->value;
$CSRFToken = $CSRFTokenModel->getCSRFToken(); 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_login); // отправляем на
        curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); // таймаут4
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // просто отключаем проверку сертификата
        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie.txt');
        curl_setopt($ch, CURLOPT_POST, 1); // использовать данные в post
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            '_csrf' => $CSRFToken,
            'LoginForm[username]' => $username,
            'LoginForm[password]' => $password,
            'login-button' => '',
        ));

        $data = curl_exec($ch);
        curl_close($ch);
        if ($this->auth_check($data)) {
            return TRUE;
        }

        return FALSE;
    }

}
