<?php defined('SYSPATH') or die('No direct script access.');

class Model_Regcode extends ORM 
{

   // protected $_table_name = 'regcode';
	public function rules()
	{
			return array(
				'code' => array(
				array('not_empty'),
				array(array($this, 'bad_code')),
				),
			);    
	}
  public function savecode($data){

		if(!$data) return false;
		
		foreach($data as $k=>$v){
			$key[] = $k;
			$value[] = $v;
		}
		
		$id = DB::insert('regcodes', $key)->values($value)->execute();
		
		return $id;
	}
        public function generateCode($number)
	{
            //$number - кол-во символов в коде
            $arr = array('a','b','c','d','e','f',
			'g','h','i','j','k','l',
			'm','n','o','p','r','s',
			't','u','v','x','y','z',
			'A','B','C','D','E','F',
			'G','H','I','J','K','L',
			'M','N','O','P','R','S',
			'T','U','V','X','Y','Z',
			'1','2','3','4','5','6',
			'7','8','9','0');

            // Генерируем код
		$code = "";
		for($i = 0; $i < $number; $i++)
		{
		// Вычисляем случайный индекс массива
			$index = rand(0, count($arr) - 1);
			$code .= $arr[$index];
		}
		return $code;
  	}
	
	public function bad_code($code)
    {
        $regcodetemp = ORM::factory('regcode', array('code'=>$code));
		
		if($regcodetemp->loaded())
		{
			if($regcodetemp->user_id == NULL)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
    }

    
    public function disactive_code($code, $user_id_reg)
    {

         $regcodetemp = ORM::factory('regcode', array('code'=>$code));
         $regcodetemp->user_id = $user_id_reg;
         $regcodetemp->save();
    }
    
    public function list_code($id=null)
    {
        $codesarr = array();
        $user = new Model_Myuser;
        	$auth = Auth::instance();
		$userId = $auth->get_user();
                $codes=ORM::factory('regcode')
                        ->where('add_user_id', '=', $userId)
                        ->find_all();
if(isset($id) && $id !== NULL){
    $codes = ORM::factory('regcode')
                        ->where('id', '=', $id[0])
                        ->find_all();
}                
                foreach ($codes as $c) {
if($user->getusername($c->user_id)){
    $status = 'Зарегистрирован пользователь '. $user->getusername($c->user_id);
}
 else {$status = 'Не использован';}
                    $codesarr[]=array(
                        'id' => $c->id,
                        'code' => $c->code,
                        'user_id' => $c->user_id,
                        'add_user_id' => $c->add_user_id,
                        'status' => $status,
                    );
                    
                    
                }
                return $codesarr;
    }
    public function delete_code($code_id){
        
        $code = ORM::factory('regcode',$code_id);
        $userlog = Auth::instance()->get_user();
    
        if($code->user_id==null && $code->add_user_id == $userlog){
         $code->delete(); 
         return FALSE;
        }
 else {
     return $code_id;
 }
 
        


    }
    
    



}
