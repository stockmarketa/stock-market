<? foreach ($allNews as $value): ?>
    <div class="news_container">
        <div class="news_date"><?=rus_date('j F Y',strtotime(date($value->date)));  ?></div>
        
        <div class="news_name"><?= $value->title ?></div>
        <hr>
        <div class="news_text"><?= $value->body ?></div>
    </div>
<? endforeach; ?>
