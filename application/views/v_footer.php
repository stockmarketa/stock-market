<div class="footer">    
	<div class="bottom_bg">© Copyright  <a href="/">StockMarket</a>  2015 - 2016</div>    
</div> 
<script type="text/javascript">

    $(function () {

  // Toggle loading container when ajax call
  var loadingContainer = $('#overlay_loader', this.el);
  loadingContainer.ajaxStart(function () {
    $("#overlay_loader").fadeIn();
  });
  loadingContainer.ajaxStop(function () {
    $("#overlay_loader").fadeOut();
  });
  
  // Ajax call
  
});
</script>    
<script type="text/javascript">
    $("#search").submit(function () {


        var form = $(this);
        $.ajax({// описываем наш запрос
            type: "POST", // будем передавать данные через POST
            url: form.attr('action'), // берем адрес отправки формы и передаем туда наши данные аяксом
            data: form.serialize(), // серриализируем данные
            dataType: "json", // указываем, что нам вернется JSON
				beforeSend: function(){
                                 $("#shader").fadeIn();// показываем индикатор загрузки
				},
            success: function (data) {
                
                if (!data.error) {
                             $('#overlay').fadeIn(400,
                            function () {
                                $("#modal_form") // берем стрoку с селектoрoм и делaем из нее jquery oбъект
                                        .css('display', 'block')
                                        .animate({opacity: 1, top: '50%'}, 200); // плaвнo пoкaзывaем
                            });
                    
                           // $("#to_cart").html.href(data.id_img);
                            $('#to_cart').attr('href', '/main/addtocart/'+data.id);
                            $('#image_pre').attr('src',data.preview_rout);
                            $('#id_shutterstock').html('ID '+data.name);
                            $('#large').html(data.size);
                            $("label[for='large']").text(data.size.replace("&sim;", "~"));
                           
                }
                else {


alert(data.message); 


                }

            },
            error: function () { // Если сервер вернул ошибку, 4хх, 5хх
                /** Выводим ошибку, делаем кнопку отправки снова активной и убираем индикатор загрузки */
                $("#error").html('Произошла ошибка').slideDown();
                $("[type=submit]", form).removeAttr('disabled');
                $("#loading").slideUp();
            }
        });

        return false;
    });

</script>

<script type="text/javascript">
    $(document).ready(function () {


        $("#to_cart").click(function (event) {
            event.preventDefault();

            var link = $(this);
            $.ajax({// описываем наш запрос
                type: "POST", // будем передавать данные через POST
                url: link.attr('href'), // берем адрес отправки формы и передаем туда наши данные аяксом
                //data: form.serialize(), // серриализируем данные
                dataType: "json", // указываем, что нам вернется JSON
//				beforeSend: function(){
//					$("#loading").slideDown(); // показываем индикатор загрузки
//				},
                success: function (data) {
                    if (!data.error) {
                        $("#count_cart").html(data.count_of_cart);
                    }
                    else{
                        
                    }
//alert (data.message);
                },
                error: function () { // Если сервер вернул ошибку, 4хх, 5хх
                    /** Выводим ошибку, делаем кнопку отправки снова активной и убираем индикатор загрузки */
                    $("#error").html('Произошла ошибка').slideDown();
                    $("[type=submit]", form).removeAttr('disabled');
                    $("#loading").slideUp();
                }
            });

            $("#image_pre")
                    .clone()
                    .css({'position': 'absolute', 'z-index': '100'})
                    .appendTo("#to_cart")
                    .animate({opacity: 0.5,
                        marginTop: -200, /* Важно помнить, что названия СSS-свойств пишущихся 
                         через дефис заменяются на аналогичные в стиле "camelCase" */
                        width: 0,
                        height: 0}, 600, function () {
                        $(this).remove();
                        $('#modal_form')
                                .animate({opacity: 0, top: '45%'}, 200,
                                        function () {
                                            $(this).css('display', 'none');
                                            $('#overlay').fadeOut(400);
                                           
                                        }
                                );
                    });

            // $.html.animate( scrollTo(0,0));
            $("html, body").animate({scrollTop: $('#top_for_scroll').offset().top}, 600);
        });



    });
</script>

<script type="text/javascript">
    $('#modal_close, #overlay').click(function () {
        $('#modal_form')
                .animate({opacity: 0, top: '45%'}, 200,
                        function () {
                            $(this).css('display', 'none');
                            $('#overlay').fadeOut(400);
                        }
                );
    });
</script>