<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="/media/css/style.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script> 
    </head>
    <body>
        <div class="wrap">
            <? if (isset($block_menu)): ?>
                <div class="menu_bg">
                    <?= $block_menu ?>
                </div>
            <? endif ?> 	
            <div class="header"></div>
            <? if (isset($block_text_intro)): ?>
                <?= $block_text_intro ?>
            <? endif ?> 	
            <? if (isset($block_container)): ?>
                <? foreach ($block_container as $bcontainer): ?>
                    <div class="container">
                        <?= $bcontainer ?>
                    </div>
                <? endforeach ?>
            <? endif ?> 
            <div class="clearfloat"></div>
            <div class="empty"></div>     
        </div>
        <? if (isset($block_footer)): ?>
            <?= $block_footer ?>
        <? endif ?>
    </body>
</html>