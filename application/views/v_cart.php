<hr class="dark4px">
<div id="error" style="display: none;"><center></center>></div>
<div class="container">
    <div class="content_name">Корзина</div>
    <form action="cart/pay" method="post"  id="cart_form">
    <table class="table" >
        <thead>
            <tr>
                <th class="col_1_cart"><div class="del_th"></div</th>
        <th class="col_2_cart">Предпросмотр</th>
        <th class="col_3_cart">Размер/Тип</th>
        <th class="col_4_cart">ID</th>
        <th class="col_5_cart">Цена</th>
        </tr>
        </thead>
       <tbody id="cart_table">
        <? foreach ($result as $r): ?>

            <tr>
                <td><input type="hidden" name="id_image[]" value="<?= $r['id'] ?>"><a href="/cart/delete/<?= $r['id'] ?>" class="del delete"></a></td>
                <td><a rel="simplebox" href="<?= $r['preview_rout'] ?>"><img class="small_zoom" src="<?= $r['preview_rout'] ?>" alt=""></a></td>
                <td><?= $r['size'] ?></td>
                <td><?= $r['name'] ?></td>
                <td>0.4 USD</td>	
            </tr>
            
        <? endforeach; ?>
        </tbody>
    </table>
       
        <button type="submit" name="pay" class="submit_buy">КУПИТЬ</button>
      
</form>

</div>
<script type="text/javascript" src="/media/js/delPayImgInCart.js"></script>
<script type="text/javascript" src="/media/simplebox/simplebox_util.js"></script>
<script type="text/javascript">
    (function () {
        var boxes = [], els, i, l;
        if (document.querySelectorAll) {
            els = document.querySelectorAll('a[rel=simplebox]');
            Box.getStyles('simplebox_css', '/media/simplebox/simplebox.css');
            Box.getScripts('simplebox_js', '/media/simplebox/simplebox.js', function () {
                simplebox.init();
                for (i = 0, l = els.length; i < l; ++i)
                    simplebox.start(els[i]);
                simplebox.start('a[rel=simplebox_group]');
            });
        }
    })();</script>



