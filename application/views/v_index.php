<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="description" content=""/>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="/media/css/style.css">
        <link href="/media/css/jquery.bxslider.css" rel="stylesheet">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script> 
        <script type="text/javascript" src="/media/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="/media/js/slider.js"></script>
    </head>
    <body>
        <div id="top_for_scroll"></div>
        <div class="wrap">
            <? if (isset($block_menu)): ?>
                <div class="menu_bg">
                    <?= $block_menu; ?>
                </div>
            <? endif ?>
            <div class="header"></div>
            <? if (isset($user_bg)): ?>
                <div class="user_bg">
                    <?= $user_bg; ?>
                </div>
            <? endif ?>
            <? if (isset($block_content)): ?>
                <? foreach ($block_content as $b_content): ?>
                    <?= $b_content ?>
                <? endforeach ?>
            <? endif ?>
            <div class="clearfloat"></div>
            <div class="empty"></div>      
        </div>
        <?= $block_footer ?>
    </body>
</html>