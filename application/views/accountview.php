<? if (isset($newpassok)) { ?>
    <p style="text-align:center; color:green;">
        Новый пароль успешно сохранен
    </p>
<? } ?>

<? if (isset($data)) { ?>
    <? foreach ($data as $item) { ?>
        <p style="text-align:center; color:red"><?= $item ?></p>
    <? } ?>
<? } ?>

<hr class="dark4px">


<div class="container">
    <div class="content_name">Личные данные</div>
    <p>
        <input type="checkbox" id="showpassbtn"><label for="showpassbtn"> Не прятать пароль за звездочки</label>
    </p>  
    <form class="contact_form" action="" method="post">
        <p>
            <label class="label-input" for="oldpass">Текущий пароль:</label>
            <input type="password" name="oldpass" id="oldpass" placeholder="" class="input-text" required />
            <span style="display: none" id="oldpassok"><img src="/media/img/ok.png" title="Старый пароль введен правильно" alt="Старый пароль введен правильно"></span>
            <span style="display: none" id="oldpasserror"><img src="/media/img/error.png" title="Ошибка в старом пароле" alt="Ошибка в старом пароле"></span>
        </p>            
        <p>
            <label class="label-input" for="email">Email:</label>
            <input type="email" name="email" placeholder="<?= $info['email'] ?>" value="<?= $info['email'] ?>" class="input-text" required />
        </p>

        <p>
            <label class="label-input" for="newpass1">Новый пароль:</label>
            <input type="password" name="newpass1" id="newpass1" placeholder="" class="input-text" required />
        </p>

        <p>
            <label class="label-input" for="newpass2">Повторите новый пароль:</label>
            <input type="password"  name="newpass2" id="newpass2" placeholder="" class="input-text" required />
            <span style="display: none" id="newpassmatchesok"><img src="/media/img/ok.png" title="Пароли совпадают" alt="Пароли совпадают"></span>
            <span style="display: none" id="newpassmatcheserror"><img src="/media/img/error.png" title="Пароли несовпадают" alt="Пароли несовпадают"></span>
        </p>


        <p>
            <label class="chk"><input type="checkbox" /> Уведомлять по почте о новостях</label>
        </p>

        <p>
            <label class="chk"><input type="checkbox" /> Уведомлять по почте о новых сообщениях </label>
        </p>   

        <p>
            <button class="submit" type="submit" name="btnsubmit">СОХРАНИТЬ ИЗМЕНЕНИЯ</button>
        </p>	

    </form>	
</div>
<script type="text/javascript">
    function checkOldPass()
    {
        var oldpass = $("#oldpass").val();

        $.ajax({
            type: "POST",
            data: "oldpass=" + oldpass,
            url: "/ajax/checkOldPass",
            dataType: "json",
            success: function (data)
            {
                if (data.result)
                {
                    $("#oldpassok").css('display', 'inline');
                    $("#oldpasserror").css('display', 'none');
                } else
                {
                    $("#oldpasserror").css('display', 'inline');
                    $("#oldpassok").css('display', 'none');
                }
            }
        })
    }

    function showPass()
    {
        var checked = $("#showpassbtn").attr('checked');

        if (checked == "checked")
        {
            document.getElementById('oldpass').type = 'text';
            document.getElementById('newpass1').type = 'text';
            document.getElementById('newpass2').type = 'text';
        } else
        {
            document.getElementById('oldpass').type = 'password';
            document.getElementById('newpass1').type = 'password';
            document.getElementById('newpass2').type = 'password';
        }
    }

    function matchesPass()
    {
        if ($("#newpass1").val() == $("#newpass2").val())
        {
            $("#newpassmatchesok").css('display', 'inline');
            $("#newpassmatcheserror").css('display', 'none');
        } else
        {
            $("#newpassmatcheserror").css('display', 'inline');
            $("#newpassmatchesok").css('display', 'none');
        }
    }

    $(document).ready(function () {
//		$("#oldpass").blur(checkOldPass);
//		$("#oldpass").keyup(checkOldPass);
        $("#oldpass").keyup(checkOldPass);
        $("#showpassbtn").click(showPass);
        $("#newpass2").keyup(matchesPass);
        $("#newpass1").keyup(matchesPass);
    });
</script>
