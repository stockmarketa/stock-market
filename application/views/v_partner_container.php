
<script src="../../media/js/script.js" type="text/javascript"></script>
<hr class="dark4px">
<div class="page_name">Партнерская программа</div>

<div class="page_text">    
    Друзья, теперь вы можете зарабатывать с помощью партнерской программы сервиса <span class="red_b">stock</span><span class="black_b">market</span>.<br>
    Все очень просто - вы получаете на баланс 10% от всех пополнений пользователей,<br> 
    которые были зарегистрированы по вашим приглашениям.<br><br>

    Регистрация новых пользователей возможна только по приглашениям.
    Приглашения могут создавать только зарегистрированные пользователи.<br><br>

    Для того, чтобы зарегистрировать нового пользователя вам нужно создать новое приглашение и передать код приглашения новому пользователю, который нужно будет ввести на странице регистрации.<br><br>

    Ссылка для регистрации <a href="#" id="url_reg"><?= $urlreg ?></a> <button onclick="copyToClipboard('#url_reg')" type="button" class="copyButton">
                            <img class="clippy" src="/media/img/clippy.svg" width="13" alt="Copy to clipboard">
                        </button>

</div>



<div class="container">
    <div class="content_name">Приглашения</div>



    <table class="table code">
        <thead>
            <tr>
                <th class="col_1_partner">Код приглашения</th>
                <th class="col_2_partner">Статус</th>
                <th class="col_3_partner" style="border-right: 1px solid #fff;"></th>
                <th class="col_3_partner"><div class="del_th"></div></th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($allcode as $value): ?>
                <tr>
                    <td><p id="<?= $value['code'] ?>"><?= $value['code'] ?></p></td>
                    <td><?= $value['status']?></td>
                    <td><?if(!$value['user_id']):?>
                        <button onclick="copyToClipboard('#<?= $value['code'] ?>')" type="button" class="copyButton">
                            <img class="clippy" src="/media/img/clippy.svg" width="13" alt="Copy to clipboard">
                        </button>
                        <? endif;?></td>
                    <td><?=!$value['user_id']?'<a href="/partner/deletecode/'.$value['id'].'" class="del"></a>':''?></td>
                </tr>
            <? endforeach; ?>	

        </tbody>
    </table>


    <a href="/partner/add" class="button" id="add">СОЗДАТЬ НОВОЕ ПРИГЛАШЕНИЕ</a>	

    <div class="white_bg_text">
        <div class="page_text">    
            Общее количество пользователей, зарегистрированных по вашим приглашениям: 4, активных: 4<br>
            Общая сумма заработанных средств: <span class="red_b">22.37 USD</span> 	
        </div>
    </div>
</div>
<script type="text/javascript">
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }
</script>