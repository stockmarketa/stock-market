$(document).ready(function(){
	$('body').on('click','#add',function(){ // при отправке формы проводим валидацию на заполненые поля и отправляем форму
		
		var link = $(this);
		
		$.ajax({ // описываем наш запрос
				type: "POST", // будем передавать данные через POST
				url: link.attr('href'), // берем адрес отправки формы и передаем туда наши данные аяксом
				//data: form.serialize(), // серриализируем данные
				dataType: "json", // указываем, что нам вернется JSON
				beforeSend: function(){
					$("#loading").slideDown(); // показываем индикатор загрузки
				},
				success: function(data) { // когда получаем ответ
                           
					if(!data.error){ // Если ошибки нет, то выводим список статей
						// Если есть статьи - то будем их выводить
						if(data.content){
							var html = '';
							for(var A in data.content){
								var item = data.content[A];
								html += '<tr><td>'+item.code+'</td><td>'+item.status+
                                                                        '</td><td><button onclick="copyToClipboard(#'+item.code+')" type="button" class="copyButton"><img class="clippy" src="https://clipboardjs.com/assets/images/clippy.svg" width="13" alt="Copy to clipboard"></button></td><td><a href="/partner/deletecode/'+item.id+'" class="del"></a></td></tr>';
							}
							$('table.code tbody').append(html);
						
						}
						
					}else{ // Если сервер вернул ошибку то выводим текст ошибки
						$("#error").html(data.message).slideDown();
					}
					
					/** Делаем кнопку отправки снова активной и убираем индикатор загрузки */
					$("[type=submit]",form).removeAttr('disabled');
					$("#loading").slideUp();
				},
				error: function(){ // Если сервер вернул ошибку, 4хх, 5хх
					/** Выводим ошибку, делаем кнопку отправки снова активной и убираем индикатор загрузки */
					$("#error").html('Произошла ошибка').slideDown();
					$("[type=submit]",form).removeAttr('disabled');
					$("#loading").slideUp();
				}
		});
		
		return false;
	});
	
	/** Функция удаления статьи */
	$('body').on('click','.del',function(){
		var link = $(this);
		if(confirm('Вы действительно хотете удалить?')){
			$.ajax({ 
				type: "POST",
				url: link.attr('href'), // берем адрес из ссылки
				dataType: "json",
				success: function(data) { // когда получаем ответ
                                   
					if(!data.error){ // Если ошибки нет, то удаляем строку
						link.closest('tr').hide(function(){
                                                    
							$(this).remove();
						})
					}else{ // Если сервер вернул ошибку то выводим текст ошибки
						$("#error").html(data.message).slideDown();
					}
				}
			});
		}
		return false;
	});
});