$(document).ready(function(){
	/** Функция удаления статьи */
	$('body').on('click','.del',function(){
		var link = $(this);
		if(confirm('Вы действительно хотете удалить?')){
			$.ajax({ 
				type: "POST",
				url: link.attr('href'), // берем адрес из ссылки
				dataType: "json",
				success: function(data) { // когда получаем ответ
                                   
					if(!data.error){ // Если ошибки нет, то удаляем строку
						link.closest('tr').hide(function(){
							$(this).remove();
                                                        $("#count_cart").html(data.count_of_cart); 
						})
                                                                                                   
					}else{ // Если сервер вернул ошибку то выводим текст ошибки
						$("#error").html(data.message).slideDown();
					}
				}
			});
		}
		return false;
	});
$(document).ready(function(){
	$("#cart_form").submit(function () {
		var form = $(this);
                
		if(confirm('Вы действительно хотете купить?')){
			$.ajax({ 
				type: "POST",
				url: form.attr('action'), // берем адрес из ссылки
                                data: form.serialize(),
				dataType: "json",
				success: function(data) { // когда получаем ответ
                                   
					if(!data.error){ // Если ошибки нет, то удаляем строку
						
							
                                                        $("#count_cart").html(data.count_of_cart); 
                                                        $("#cart_table").empty();
						
                                                                                                   
					}else{ // Если сервер вернул ошибку то выводим текст ошибки
						$("#error").html(data.message).slideDown();
					}
				}
			});
		}
		return false;
	});
});
});