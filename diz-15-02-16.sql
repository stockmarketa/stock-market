-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.41 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица diz.format
CREATE TABLE IF NOT EXISTS `format` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.format: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `format` DISABLE KEYS */;
INSERT INTO `format` (`id`, `name`, `value`) VALUES
	(1, 'Small', NULL),
	(2, 'Medium', NULL),
	(3, 'Large', NULL);
/*!40000 ALTER TABLE `format` ENABLE KEYS */;


-- Дамп структуры для таблица diz.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `rout` text,
  `preview_rout` text,
  `format_info` text,
  `shopdiz_id` int(20) DEFAULT NULL,
  `resolution` text,
  `stock_id` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.images: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`id`, `name`, `rout`, `preview_rout`, `format_info`, `shopdiz_id`, `resolution`, `stock_id`) VALUES
	(63, '209948746', NULL, 'photos/shutterstock/preview/preview-209948746.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-209948746.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 6000x4000 | approx.&sim;7.3 MB"}', 1053942, NULL, 1),
	(65, '123', NULL, 'photos/shutterstock/preview/preview-123.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-123.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.&sim;13.9 MB"}', 1053874, NULL, 1),
	(66, '221488444', NULL, 'photos/shutterstock/preview/preview-221488444.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-221488444.html","cost":"dl_1","code":"huge_jpg","size":"Large | 4928x3264 | 2.6 MB"}', 1053693, NULL, 1),
	(67, '319361252', NULL, 'photos/shutterstock/preview/preview-319361252.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-319361252.html","cost":"dl_1","code":"huge_jpg","size":"Large | 4791x3264 | 9.0 MB"}', 1053392, NULL, 1),
	(68, '218601670', NULL, 'photos/shutterstock/preview/preview-218601670.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-218601670.html","cost":"dl_1","code":"huge_jpg","size":"Large | 7000x3957 | 18.3 MB"}', 1053908, NULL, 1),
	(71, '289903277', NULL, 'photos/shutterstock/preview/preview-289903277.jpg', '{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-289903277.html","cost":"dl_1","code":"vector_eps","size":"Vector | vector"}', 1046493, NULL, 1);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;


-- Дамп структуры для таблица diz.images_users
CREATE TABLE IF NOT EXISTS `images_users` (
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.images_users: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `images_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `images_users` ENABLE KEYS */;


-- Дамп структуры для таблица diz.menumain
CREATE TABLE IF NOT EXISTS `menumain` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `url` varchar(150) NOT NULL,
  `login` int(2) NOT NULL,
  `no_login` int(2) NOT NULL,
  `sort` int(2) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.menumain: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `menumain` DISABLE KEYS */;
INSERT INTO `menumain` (`id`, `name`, `url`, `login`, `no_login`, `sort`, `status`) VALUES
	(1, 'ГЛАВНАЯ', '', 1, 1, 1, 1),
	(2, 'КОЛЛЕКЦИИ ИЗОБРАЖЕНИЙ', 'collection', 1, 0, 2, 1),
	(3, 'ЦЕНЫ', 'prices', 1, 0, 3, 1),
	(4, 'НОВОСТИ', 'news', 1, 0, 4, 1),
	(5, 'ОБРАТНАЯ СВЯЗЬ', 'feedback', 1, 1, 5, 1),
	(6, 'ВХОД', 'auth', 0, 1, 6, 1),
	(7, 'РЕГИСТРАЦИЯ', 'auth/registration', 0, 1, 7, 1);
/*!40000 ALTER TABLE `menumain` ENABLE KEYS */;


-- Дамп структуры для таблица diz.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `count_img` int(11) DEFAULT NULL,
  `total_sum` decimal(4,2) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.orders: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user_id`, `count_img`, `total_sum`, `order_status_id`) VALUES
	(1, 35, 2, 2.00, 1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Дамп структуры для таблица diz.order_images
CREATE TABLE IF NOT EXISTS `order_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`order_id`,`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.order_images: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `order_images` DISABLE KEYS */;
INSERT INTO `order_images` (`id`, `order_id`, `image_id`) VALUES
	(1, 1, 65),
	(2, 1, 63);
/*!40000 ALTER TABLE `order_images` ENABLE KEYS */;


-- Дамп структуры для таблица diz.order_status
CREATE TABLE IF NOT EXISTS `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.order_status: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;


-- Дамп структуры для таблица diz.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.payment: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


-- Дамп структуры для таблица diz.payment_method
CREATE TABLE IF NOT EXISTS `payment_method` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `url` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.payment_method: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
INSERT INTO `payment_method` (`id`, `name`, `url`, `logo`, `status`) VALUES
	(1, 'robokassa', '', '/media/img/robokassa.jpg', 1),
	(2, 'interkassa', '', '/media/img/interkassa.jpg', 1),
	(3, 'pay2pay', '', '/media/img/pay2pay.jpg', 1),
	(4, 'pfmoney', '', '/media/img/pfmoney.jpg', 1),
	(5, 'referal', '', '', 0);
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;


-- Дамп структуры для таблица diz.regcodes
CREATE TABLE IF NOT EXISTS `regcodes` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `add_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.regcodes: ~28 rows (приблизительно)
/*!40000 ALTER TABLE `regcodes` DISABLE KEYS */;
INSERT INTO `regcodes` (`id`, `code`, `user_id`, `add_user_id`) VALUES
	(8, '4', 32, 1),
	(18, 'eLHAZEIG85JHdH69nMBVd4DzOG43KraK9vPJzDop', 33, 1),
	(23, 'EFf4joYSXPyRehs7oCHcb9IeNHtOYGr3STCsgHpc', NULL, 33),
	(24, 'mVSSUVox5x1NA78LFtJ251kEYvRf7lMVGm6frVdk', NULL, 33),
	(25, 'BPbMProFpCTA4G6DXuZoNn22Sca2SiVx1ve9ZaAX', NULL, 33),
	(67, 'GvsXDVcDrFMUtbk2yroF3V87JA414UiBLPsCgSDh', NULL, 1),
	(72, 'NBCFt4Yjlprna6uZ8R9EU6MvMCpasMyH9s8IzYzh', NULL, 1),
	(73, 'lOXbvCIUI1SCt6A8IsBKVd9n6XTlDJ7vzctbjofd', NULL, 1),
	(74, 'mZnirBRTjICMkNJblOPOHf2Ud4RagHrNF0mzMSKE', NULL, 1),
	(75, 'v1OMHAL5gFIgN12ToLsSGHrHI4Nk7tRgUfsrL5Yj', NULL, 1),
	(77, 'HVy70Az0f7KSnYbuo9sIC6GLSMslyHRIrNsyVyo7', NULL, 1),
	(78, 'PVf4xo8EOVzC61I90y0Yl6YiyyKS6oCUME073IpG', NULL, 1),
	(79, 'DC4mVM8nMcdryksL40p6C9cB9do2UU7ANaU20rTv', NULL, 1),
	(80, 'NIFN8sk8UEtaxJa0PG4OaZamlB9xKZ4vc8u44tAk', NULL, 1),
	(81, '92T8NcvAU34uMVB3gTJUInznZnH0mlcpKfGXtzdU', NULL, 1),
	(82, 'mer3MKnyA6Ib5Gt98yg958cnJmPDvMj6cjcsarAY', NULL, 1),
	(83, 'K9dTtbyC2rNsXDjUnXZUX86VGmtYdpxrDt6mvgrd', NULL, 1),
	(84, 'UZ9YL1kfS3ArGc7mc8BnROhkEYUz3BFTYkgGVgYL', NULL, 1),
	(85, 'TZrvjAXJbEaNYhlyB7ciB0XXT5hLYUsFMibYOjFt', NULL, 1),
	(86, 'KG7fP3HCnejtnXG0hcZx4DYUajOaARhDMjB0Jkr4', NULL, 1),
	(87, 'Z2vVApAZsIxjbDFsPmSl4EtsSHiKVdJGirAfB7cU', NULL, 1),
	(109, 'eKbEzvJMEEEJ7UgZZtpsU8uEY1ixJSczjX3JjV8t', 35, 32),
	(110, 'bJj3R1ZgDorIjXj6h5HAmKcXUtSnKbkCfmGRd1vP', NULL, 35),
	(116, 'tYUsg6xGEIh2AsTNynATv85DXHzSOLMVApZFedOr', NULL, 32),
	(118, 'TNRkRgCUuhbYpCTZKsAX4xCio5O4pKzDcPE2j3sG', NULL, 35),
	(119, 'Vku6gYREuAIDbVoxseLiTcaoz6oP7yS3APnZBGi0', NULL, 35),
	(120, 'ehsu8mbvial9nmhRuePDcrlXBdFKhh8xlTzeoHll', NULL, 35),
	(121, 's8n4z9kKxku9SBxD2pmYSLAyNOLdjeGDcUvCTHdg', NULL, 32);
/*!40000 ALTER TABLE `regcodes` ENABLE KEYS */;


-- Дамп структуры для таблица diz.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.roles: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `description`) VALUES
	(1, 'login', 'Login privileges, granted after account confirmation'),
	(2, 'admin', 'Administrative user, has access to everything.');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Дамп структуры для таблица diz.roles_users
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.roles_users: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
	(1, 1),
	(32, 1),
	(33, 1),
	(35, 1);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Дамп структуры для таблица diz.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.settings: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `name`, `value`) VALUES
	(1, 'Цена за фото', '1.00');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


-- Дамп структуры для таблица diz.shopdiz
CREATE TABLE IF NOT EXISTS `shopdiz` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.shopdiz: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `shopdiz` DISABLE KEYS */;
INSERT INTO `shopdiz` (`id`, `name`, `value`, `description`) VALUES
	(1, 'url_login', 'https://shopdiz.biz/user/login', 'Адрес отправки формы авторизации'),
	(2, 'username', 'egoruz', 'Имя пользователя (логин)'),
	(3, 'password', 'ERER663752ERER', 'Пароль пользователя'),
	(4, 'url_add_to_cart', 'https://shopdiz.biz/cart/add/shutterstock', 'Адрес добавления в корзину массивом\r\n{"type":"image","url":"http:\\/\\/www.shutterstock.com\\/pic-123.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.∼13.9 MB"}'),
	(5, 'url_parse_size_add_to_cart', 'https://shopdiz.biz/ajax/multiGetSizes', NULL),
	(6, 'post2_add_to_cart', '.html","cost":"dl_1","code":"supersize_jpg","size":"Super | 3840x5120 | approx.∼13.9 MB"}', NULL);
/*!40000 ALTER TABLE `shopdiz` ENABLE KEYS */;


-- Дамп структуры для таблица diz.stocks
CREATE TABLE IF NOT EXISTS `stocks` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `dir` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.stocks: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
INSERT INTO `stocks` (`id`, `name`, `url`, `dir`) VALUES
	(1, 'shutterstock', 'http://www.shutterstock.com/', 'photos/shutterstock'),
	(2, 'shopdiz', 'https://shopdiz.biz/', 'photos/shopdiz');
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;


-- Дамп структуры для таблица diz.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `cart` text,
  `balance` decimal(5,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.users: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `parent_id`, `cart`, `balance`) VALUES
	(1, 'eg2or.uzh@gmail.com', 'Egor1', '116a4040cab7507da9bcdd2f83139312fbeb9fbccc0ca35125458883af70a52b', 17, 1453074079, NULL, NULL, 0.00),
	(32, '11eqgor.uzh@gmail.com', 'Egor', '116a4040cab7507da9bcdd2f83139312fbeb9fbccc0ca35125458883af70a52b', 55, 1455303686, NULL, '65,', 0.00),
	(33, 'egor.uzh@gmail.com', 'egor1q', '983691638a6bda2477028968a3318ff6f18f2a09de81d77952017438a554074e', 1, 1453050720, NULL, '65,66,67,', 0.00),
	(35, 'egor-y@yandex.ru', 'egor3', '116a4040cab7507da9bcdd2f83139312fbeb9fbccc0ca35125458883af70a52b', 17, 1455304796, NULL, '65,63,', 7.00);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Дамп структуры для таблица diz.user_tokens
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `type` varchar(100) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы diz.user_tokens: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
